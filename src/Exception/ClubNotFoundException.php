<?php
/**
 * Created by Antoine Lamirault.
 */

namespace FFTTApi\Exception;


class ClubNotFoundException extends \Exception
{
    public function __construct($clubId)
    {
        parent::__construct(
            sprintf(
                "Le club avec l'id '%s' n'existe pas.",
                $clubId
            )
        );
    }
}